package main

import (
	"fmt"
	"math"
)

// все наименования в PascalCase (GolangProject) или camelCase (golangProject)

const (
	a       = 1
	b int16 = 2
	c int32 = 3
	d int64 = 323435

	ua uint8  = 1
	ub uint16 = 2
	uc uint32 = 3
	ud uint64 = 323435

	f32 float32 = 23.3
	f64 float64 = 23.3

	bb byte = 10 // uint8

	r rune = 'M' // uint32

	s string = "golang"

	isBool bool = true // or false

	co64  complex64  = 1 + 2i
	co128 complex128 = 1 + 3i
)

type tType string

const (
	aa = 1
	ss = "go" // string and tType
)

const (
	ia = 2
	ib4
	ic = iota
	id
)

const ids = 0

const (
	bia = iota << 1
	bib
	bic
	bid
)

const (
	_ = iota << 1
	bib2
	_
	bid2
)

func main() {
	fmt.Println(
		math.MinInt64,
	)

}
