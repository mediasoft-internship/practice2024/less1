package main

import "fmt"

func main() {
	var (
		a       = 10
		b int64 = 20
	)

	b = int64(a)

	fmt.Println(b)

	// Кастинг кастомных типов
	// type custStr string

	type H struct {
		a int
		f float32
		s string
	}

	anon := struct {
		a int
		f float32
		s string
	}{
		0, 1.0, "Go",
	}

	h := H{}
	h = anon

	fmt.Println(h)
}
