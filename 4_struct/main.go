package main

import "fmt"

type employee struct {
	Name     string // имя
	Age      int    // возраст
	Position string // позиция
	Salary   int    // зарплата
}

func main() {
	var (
		empl    employee
		emplPtr *employee
	)

	fmt.Println(emplPtr)

	empl.Age = 25
	empl.Position = "Программист"
	empl.Name = "Гофер"
	empl.Salary = 10000

	emplPtr = &empl
	empl.Salary = 100
	emplPtr.Age = 26

	fmt.Println(*emplPtr, empl)

	empl2 := employee{
		"Гофер", 20, "Директор", 10000,
	}

	empl2 = employee{
		Name:     "Гофер",
		Age:      20,
		Position: "Директор",
		Salary:   10000,
	}

	emplPtr2 := &employee{
		Name:     "Гофер",
		Age:      20,
		Position: "Директор",
		Salary:   100,
	}
	emplPtr3 := new(employee)

	fmt.Println(empl2, emplPtr2, emplPtr3)
}
