package main

import "fmt"

func main() {

	a := 20
	switch a {
	case 10:
		fmt.Println(10)
		fallthrough
	case 11:
		fmt.Println(11)
	case 12:
		fmt.Println(12)
	}

}
