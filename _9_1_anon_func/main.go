package main

import "fmt"

func main() {
	sum := func(i int, k int) (int, string) {
		return i + k, fmt.Sprintf("= %d + %d", i, k)
	}
	fmt.Println(sum(10, 10))
}
