package main

import (
	"fmt"
)

func main() {

	fmt.Println(sum(10, 10))
}

func sum(i int, k int) (int, string) {
	return i + k, fmt.Sprintf("= %d + %d", i, k)
}
