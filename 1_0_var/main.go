package main

// все наименования в PascalCase (GolangProject) или camelCase (GolangProject)

var (
	a int8  = 1
	b int16 = 2
	c int32 = 3
	d int64 = 323435

	ua uint8  = 1
	ub uint16 = 2
	uc uint32 = 3
	ud uint64 = 323435

	f32 float32 = 23.3
	f64 float64 = 23.3

	bb byte = 10 // uint8

	r rune = 'M' // uint32

	s string = "golang"

	isBool bool = true // or false

	co64  complex64  = 1 + 2i
	co128 complex128 = 1 + 3i
)

type tType string

var (
	aa = 1
	ss = "go" // string and tType
)

var ids = 0

func main() {
	//goVersion := "1.20"
	//yearOfBirth := 2009
	//_ = yearOfBirth
	//
	//var (
	//	language = "golang"
	//	version  string
	//	ptrYear  *int
	//)
	//ptrYear = &yearOfBirth
	//_, _ = language, version
	//
	//fmt.Println(
	//	goVersion,
	//	*ptrYear,
	//)

	////var unknownType any = 2009
	////fmt.Println(unknownType)
	//
	//unknownType = "2009"
	//fmt.Println(unknownType)
}
